package com.example.sample

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sample.databinding.FragmentFragmentaBinding
import java.util.*


class Fragmenta : Fragment() {
private lateinit var binding:FragmentFragmentaBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding=FragmentFragmentaBinding.inflate(inflater,container,false)
        return binding.root
    }
  val dateandtime:()->String={
     Calendar.getInstance().time.toString()
  }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.b1.setOnClickListener { with(binding.textView){
            when(getText()){
                "Maneesh"->text="Reddy".also { binding.b1.text="Maneesh" }
                "Reddy"->text="Maneesh Reddy".apply { binding.b1.text="Reddy" }
                "TextView"->text="Maneesh"
                else->text= dateandtime.invoke()
            }
        } }
    }
}